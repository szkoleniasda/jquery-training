(function ($) {
    $.fn.mySuperTabs = function (options) {

        var settings = $.extend({
            tabContent: "#tabContent",
            abc: "test"
        }, options);

        alert(settings.abc);

        var tabs = {
            tabContent: null,
            init: function () {
                $(".tabs li a").on("click", tabs.clickBtn);
                tabs.tabContent = $(settings.tabContent);

                tabs.tabContent.slideDown(1000, function () {
                    tabs.loadContent(
                        $(".tabs li.active a").attr("href")
                    );
                });
            },
            clickBtn: function (e) {
                $(".tabs li.active").removeClass("active");
                $(this).parent().addClass("active");

                var url = $(this).attr("href");
                console.log(url);
                tabs.loadContent(url);
                e.preventDefault();
            },
            loadContent: function (url) {
                $.ajax({
                    type: "GET",
                    url: url,
                    dataType: 'html',
                    beforeSend: tabs.showLoader()
                })
                    .always(tabs.hideLoader())
                    .fail(function () {
                        alert("Nie moge załadować danych");
                    })
                    .done(function (data) {
                        console.log(data);
                        tabs.tabContent.html(data);
                    });

            },
            showLoader: function () {
                $('#loader').show(1000);
            },
            hideLoader: function () {
                $('#loader').hide(1000);
            }
        };

        tabs.init();

    };
})(jQuery);
