(function ( $ ) {
    $.fn.mySuperTabs = function(){

        var settings = $.extend({
            tabContent: "#tabContent",
            abc: "test"
        }, options);

        alert(settings.abc);

        var showLoader = function () {
            $('#loader').show(1000);
        };

        var hideLoader =function () {
            $('#loader').hide(1000);
        };

        var loadContent = function (url) {
            $.ajax({
                type: "GET",
                url: url,
                dataType: 'html',
                beforeSend: showLoader()
            })
                .always(hideLoader())
                .fail(function () {
                    alert("Nie moge załadować danych");
                })
                .done(function (data) {
                    console.log(data);
                    tabContent.html(data);
                });
        };

        var clickBtn = function (e) {
            $(".tabs li.active").removeClass("active");
            $(this).parent().addClass("active");

            var url = $(this).attr("href");
            console.log(url);
            loadContent(url);
            e.preventDefault();
        };

        $(".tabs li a").on("click", clickBtn);

        var tabContent = $(settings.tabContent);

        tabContent.slideDown(1000, function () {
            loadContent(
                $(".tabs li.active a").attr("href")
            );
        });

    };
})(jQuery);