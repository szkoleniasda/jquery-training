$(document).ready(initialize);
var jsonObject = [];
var currentObject = 0;
function initialize() {

    var btn = $("<button>",
            {html: "kliknij!",
                id: "btn",
                type: "button"
            });
    $('body').append(btn);
    $("#btn").click(function () {
        $(this).prop("disabled", true);
        $.ajax(
                {
                    url: '/Ajax/json.json',
                    type: "GET",
                    dataType: "JSON",
                    beforeSend: loading()

                })
                .done(function (data) {
                    jsonObject = data;
                    loaded();
                    defineMovingButtons();
                    defineTable();

                });




    })

}
;
function loading()
{
    console.log("Ładuję!");
}
function loaded()
{
    console.log(jsonObject[0].id);
}
function defineMovingButtons()
{
    var btnLeft = $("<button>",
            {html: "<<",
                id: "btnL",
                type: "button"
            });
    var btnRight = $("<button>",
            {html: ">>",
                id: "btnR",
                type: "button"
            });
    $('body').append(btnLeft).append(btnRight).append("<br/>");

    $('#btnL').click(moveLeft);
    $('#btnR').click(moveRight);
    $('#btnL').prop("disabled", true);
    refresh();



}
function defineTable() {
    var uelka = $('<ul/>',
            {
                id: "uelka"
            });
    var liId = $('<li/>', {
        id: "idid"
    });
    var liName = $('<li/>', {
        id: "name"
    });

    var liPrice = $('<li/>', {
        id: "price"
    });
    var liTags = $('<li/>', {
        id: "tags"
    });
    var liDimensions = $('<li/>', {
        id: "dimensions"

    });
    var liWarehouseLocation = $('<li/>', {
        id: "warehouseLocation"

    });
    $(uelka).append(liId).append(liName).append(liPrice).append(liTags).append(liDimensions).append(liWarehouseLocation);
    $('body').append("Produkt " + "<span id='current'>" + (currentObject + 1) + "</span> z " + jsonObject.length).append("<br/>").append(uelka);

    refresh();
}
function moveLeft()
{
    currentObject--;
    refresh();
    $('#idid').text("Id:" + jsonObject[currentObject].id);
    console.log('lewo');
    if (0 === currentObject)
    {
        $('#btnL').prop("disabled", true);
    }
    $('#btnR').prop("disabled", false);
}
function moveRight()
{
    currentObject++;
    console.log(currentObject);
    refresh();
    if ((jsonObject.length - 1) === currentObject)
    {
        $('#btnR').prop("disabled", true);
    }
    $('#btnL').prop("disabled", false);
}
function refresh()
{
    $('#idid').text("Id: " + jsonObject[currentObject].id);
    $('#name').text("Name: " + jsonObject[currentObject].name);
    $('#price').text("Price: " + jsonObject[currentObject].price);
    $('#tags').text("Tags: " + jsonObject[currentObject].tags);
    $('#dimensions').html("Dimensions:" + "<br/>Weigth: " + jsonObject[currentObject].dimensions.length + "<br/>Width: " + jsonObject[currentObject].dimensions.width + "<br/>Heigth: " + jsonObject[currentObject].dimensions.height);
    $('#warehouseLocation').html("WarehouseLocation: <br/>Latitude: " + jsonObject[currentObject].warehouseLocation.latitude + "<br/>Longitude: " + jsonObject[currentObject].warehouseLocation.longitude);
    $('#current').text(currentObject + 1);

}