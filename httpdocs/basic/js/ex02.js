var exercise02 = {
    config : {
    },
    startup: function () {
        var list = $("ul#myList li").each(function (){
            var elem = $(this);
            var text = elem.text();
            var nmbr = text.substring((text.length - 3));
            elem.text(nmbr + " " + text.replace(nmbr, ""));
        });

        list.eq(0).addClass("first");

        $('#btn1').click(function(e){
            $("p").removeAttr("class").css("background-color", "#C90000");
            e.preventDefault();
        });

        $('#btn2').text('Ukryj').click(function(e){
            exercise02.toggleList($(this));
            e.preventDefault();
        });
    },
    toggleList: function (btn){
        $("ul#myList").slideToggle("slow", function (){
            if ($(this).is(':visible')){
                btn.text('Ukryj');
            }else{
                btn.text('Pokaż');
            }
        });
    }
};

$(document).ready( function(){
  exercise02.startup();
});
na