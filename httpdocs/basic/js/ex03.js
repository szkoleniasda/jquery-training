var exercise03 = {
    config : {
    },
    startup: function () {

        var body = $("body");

        var inputsHtml = '';
        for (var i=0; i < 5; i++){
            inputsHtml += '<input class="singleWord" id="word' + i + '" /><br/>';
        }

        body.html(inputsHtml);

        body.append(
            $("<textarea></textarea>",
                {
                    id: "phrase"
                }
            )
        );

        body.append(
            $("<button/>",
                {
                    id: "btn",
                    text: "Złóż zdanie"

                }
            )
        );

        $( "#btn" ).on( "click", function () {
            var correct = true;
            var phrase = '';
            $(".singleWord").each(function () {
                var inputVal = $(this).val();
                if (inputVal.length == 0){
                    correct = false;
                    return false;
                }
                phrase += inputVal + " ";
            });

            if (correct == false){
                alert ("Proszę wypełnić wszystkie pola!");
                return false;
            }

            $("#phrase").text(phrase);
        });
  }
};

$(document).ready( function(){
  exercise03.startup();
});
