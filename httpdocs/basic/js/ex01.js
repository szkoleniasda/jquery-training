var exercise = {
    config : {
    },
    startup: function () {
        $('#btn1').click(function(e){
            alert ("Obsługuje mnie jQuery");
            e.preventDefault();
        });

        var show = function(e) {
            alert ("Paragrafów na stronie: " + $('p').length + " paragrafów");
            e.preventDefault();
        };

        $( "#btn2" ).on( "click", show );

        $('h2.specialElem').on( "click", exercise.showHtml);
    },
    showHtml: function (e){
        alert ($(this).html());
        e.preventDefault();
    }
};

$(document).ready( function(){
  exercise.startup();
});
