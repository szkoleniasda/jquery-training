var exercise1 = {
    config : {
    },
    startup: function () {
        $( "#btn1" ).on( "click", exercise1.loadContent );
    },
    loadContent: function (){
        exercise1.showLoader("#loader1");
        exercise1.showLoader("#loader2");

        $.get("ajax1.txt", function(data){
            $( "#myData1" ).text(data);
            exercise1.hideLoader("#loader1");
        });

        $.get("ajax2.html", function(data){
            $( "#myData2" ).html(data);
            exercise1.hideLoader("#loader2");
        });
    },
    showLoader: function (loaderId){
        $(loaderId).show("slow");
    },
    hideLoader: function (loaderId){
        $(loaderId).hide("slow");
    }
};

$(document).ready( function(){
  exercise1.startup();
});
