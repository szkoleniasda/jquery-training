var exercise2 = {
    view : {
        id: $('<input/>'),
        name: $('<input/>'),
        price: $('<input/>'),
        tags: $('<input/>'),
        dimL: $('<input/>'),
        dimW: $('<input/>'),
        dimH: $('<input/>'),
        lat: $('<input/>'),
        long: $('<input/>'),
        counter: $('<span></span>'),
        nextBtn: $('<button>&gt;&gt;</button>'),
        prevBtn: $('<button>&lt;&lt;</button>'),
        loader: $('<div>Ładuje dane produktowe ... </div>').css("display", "none"),
    },
    products: [],
    actualProductIdx: -1,
    startup: function () {
        exercise2.generateHtmlContent();
        exercise2.setBtns();
        exercise2.view.nextBtn.on( "click", function(){
            if (exercise2.actualProductIdx < exercise2.products.length - 1){
                exercise2.actualProductIdx++;
            }
            exercise2.showActualProduct();
        });
        exercise2.view.prevBtn.on( "click", function(){
            if (exercise2.actualProductIdx > 0){
                exercise2.actualProductIdx--;
            }
            exercise2.showActualProduct();
        });

        $.ajax({
            type: "GET",
            url: "products.json",
            dataType: 'json',
            beforeSend: exercise2.showLoader
        })
        .always(exercise2.hideLoader)
        .fail(function() {
            alert( "Nie moge załadować danych" );
        })
        .done(function (json) {
            if (json.length > 0 ){
                exercise2.products = json;
                exercise2.actualProductIdx = 0;
                exercise2.showActualProduct();
            }else{
                alert("Niepoprawne dane produktowe");
            }
        });

    },
    setCounter: function () {
        exercise2.view.counter.text((exercise2.actualProductIdx + 1) + ' z ' + exercise2.products.length);
    },
    showActualProduct: function () {
        if (exercise2.actualProductIdx > exercise2.products.length - 1){
            alert("Brak produktu o ID: " + exercise2.actualProductIdx );
            return false;
        }
        var product = exercise2.products[exercise2.actualProductIdx];
        console.log(product);
        exercise2.view.id.val(product.id);
        exercise2.view.name.val(product.name);
        exercise2.view.price.val(product.price);
        exercise2.view.tags.val(product.tags.toString());
        exercise2.view.dimL.val(product.dimensions.length);
        exercise2.view.dimW.val(product.dimensions.width);
        exercise2.view.dimH.val(product.dimensions.height);
        exercise2.view.long.val(product.warehouseLocation.longitude);
        exercise2.view.lat.val(product.warehouseLocation.latitude);
        exercise2.setBtns();
        exercise2.setCounter();
    },
    generateHtmlContent: function (){
        var body = $("body");
        body.append(exercise2.view.loader);
        body.append(exercise2.view.prevBtn);
        body.append(exercise2.view.counter);
        body.append(exercise2.view.nextBtn);
        exercise2.addField(exercise2.view.id, 'ID:', body);
        exercise2.addField(exercise2.view.name, 'Nazwa:', body);
        exercise2.addField(exercise2.view.price, 'Cena:', body);
        exercise2.addField(exercise2.view.tags, 'Tagi:', body);
        exercise2.addField(exercise2.view.dimL, 'Długość:', body);
        exercise2.addField(exercise2.view.dimW, 'Szerokość:', body);
        exercise2.addField(exercise2.view.dimH, 'Wysokość:', body);
        exercise2.addField(exercise2.view.lat, 'Położenie LAT:', body);
        exercise2.addField(exercise2.view.long, 'Położenie LONG:', body);
    },
    addField: function (field, label, body){
        body.append(
            $('<label/>',
            {
                "for": field.attr('id'),
                "text": label
            }).css('display', 'block')
        );
        body.append(field);
        return field;
    },
    setBtns: function (){
        exercise2.view.nextBtn.attr('disabled','disabled');
        exercise2.view.prevBtn.attr('disabled','disabled');

        console.log(exercise2.actualProductIdx);
        var productsNum = exercise2.products.length;

        if (productsNum > 0) {
            if (exercise2.actualProductIdx > 0) {
                exercise2.view.prevBtn.removeAttr('disabled');
            }

            if (exercise2.actualProductIdx < productsNum - 1) {
                exercise2.view.nextBtn.removeAttr('disabled');
            }
        }

    },
    showLoader: function (){
        exercise2.view.loader.show("slow");
    },
    hideLoader: function (){
        exercise2.view.loader.hide("slow");
    }
};

$(document).ready( function(){
  exercise2.startup();
});
